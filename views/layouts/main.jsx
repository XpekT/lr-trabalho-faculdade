const React = require('react');
const Navigation = require('./nav')

class Main extends React.Component {

  render() {
    return (
      <html>
        <head>
          <title>Servidor Media Stream - LR</title>
          <link rel="stylesheet" type="text/css" href="/css/main.css" />
        </head>
        <body>
          <div className="container">
            <Navigation/>
            {this.props.children}
          </div>
        </body>
      </html>
    );
  }

}

module.exports = Main;
