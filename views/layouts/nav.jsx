import React, { Component } from 'react'

class Nav extends Component {

  render() {
    return (
      <nav className="navigation">
        <ul>
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/live/video">Live Video</a>
          </li>
          <li>
            <a href="/live/audio">Live Radio</a>
          </li>
          <li>
            <a href="/iptv">IPTV</a>
          </li>
        </ul>
      </nav>
    )
  }

}

module.exports = Nav
