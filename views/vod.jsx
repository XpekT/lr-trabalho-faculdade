import React, { Component } from 'react'
import Main from './layouts/main'
import Player from './player'

class VOD extends Component {

  render() {

    let { files, id } = this.props

    return (
      <Main>
        <div className="thumbnails-wrapper">
          { files.map((file, index) => {

            let filename = file.split('.')[0]

            // Check if file is a indeed a file and not a directory
            if(file.split('.')[1] !== undefined) {

              return (
                <div key={index} className="thumbnail">
                  <h2>{filename}</h2>
                  <Player file={file} id={id} />
                </div>
              )

            } else {

              return(
                <div key={index}>
                  <h2>Pasta</h2>
                  <a href={`/vod/${id}/${file}`}>{file}</a>
                </div>
              )

            }

          }) }
        </div>
      </Main>
    )
  }

}

module.exports = VOD
