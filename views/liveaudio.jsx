import React, { Component } from 'react'
import Main from './layouts/main'
import ReactAudioPlayer

from 'react-audio-player'

class LiveAudio extends Component {

  render() {

    return (
      <Main>
        <div className="live-container">
          <div className="rtmp-qr-code">
            <h1>Stream RTMP</h1>
            <img src={`/live/${streamHash}/qr_code_rtmp_link.png`} />
            <p>rtmp://{ip}/live/{streamHash}</p>
          </div>
        </div>
      </Main>
    )
  }

}

module.exports = LiveAudio
