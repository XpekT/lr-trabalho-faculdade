import React, { Component } from 'react'
import Main from './layouts/main'

class VODAudio extends Component {

  render() {

    let { files, mainFolderPath } = this.props

    return(
      <Main>
        <div className="content-wrapper">
        <a className="back" href={ mainFolderPath }>&laquo; Voltar para minha pasta principal!</a>
        {
          files.map((file, index) => {

            // Check if file is mp3 - can be played natively
            if(file.split('.')[1] === 'mp3') {

              return (
                <div key={index} className="audio">
                  <p>{ file.split('.')[0] }</p>
                  <video controls>
                    <source src={file} type="audio/mp3"></source>
                  </video>
                </div>
              )

            } else if (file.split('.')[1] === 'mp4') {

              // Check if file is mp4 - can be played natively
              return (
                <div key={index} className="video">
                  <p>{ file.split('.')[0] }</p>
                  <video controls>
                    <source src={file} type="audio/mp4"></source>
                  </video>
                </div>
              )

            } else {

              return (
                <div key={index}>
                  <a key={index} href={file} className="not-to-add-padding">{ file }</a>
                  <small>*Só ficheiros do tipo .mp3 são reproduzivéis.</small>
                </div>
              )

            }

          })
        }
        </div>
      </Main>
    )
  }

}

module.exports = VODAudio
