import React, { Component } from 'react'

class Player extends Component {

  render() {

    let { file, id } = this.props

    let path = `/vod/${id}/${file}`
    let type = file.split('.')[1]

    return (
      <div className="player">
        <video style={{ width: "100%", height: "100%" }} controls>
          <source src={path} type={`video/${type}`}></source>
        </video>
      </div>
    )
  }

}

module.exports = Player
