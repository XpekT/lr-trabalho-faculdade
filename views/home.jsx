const React = require('react')
const Main = require('./layouts/main')

class Home extends React.Component {

  render() {
    return (
      <Main>
        <div className="form-wrapper">
          <form action="/login" method="post">
            <h3>Acesso VOD</h3>
            <input type="text" name="username" placeholder="Username" />
            <input type="password" name="password" placeholder="Password" />
            <button type="submit">Entrar!</button>
            <button type="reset" style={{ marginLeft: '10px' }}>Limpar!</button>
          </form>
        </div>
      </Main>
    )
  }

}

module.exports = Home
