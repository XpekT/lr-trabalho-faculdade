# Servidor Media Stream com NodeJS + FFmpeg  
Laborat�rios de Redes - ISPGaya - RSI

### Pr�-requesitos
Static FFmpeg Build - https://www.johnvansickle.com/ffmpeg/  
Ficheiros Media - https://drive.google.com/drive/folders/1qDAbQHuRl_6ehbnl76p6j_TxsQdBWmHi  
NodeJS  
FFmpeg

### Instala��o
1. npm install

## Authors

* **Bruno Moreira**  
https://bitbucket.org/XpekT/media-server-node/src/master/
* **Nuno Lopes**  
https://drive.google.com/open?id=1wdngeBG69yGiezQK1rnBEUQdaNiVX-jI
* **Pedro Varela**  
https://drive.google.com/open?id=1LWGdz294oqkOvVmQ-n6zMBFBPqc_NdZM
* **Diogo Mateus**  
https://drive.google.com/open?id=12sSWmO0Ek9UbZuhMYdOsvHM1bMx8JuOZ
* **Diogo Alves**  
https://drive.google.com/open?id=1ILhC4OPmvyUH_ZuHD0o-mMhoWw6Y1E2x
* **Hugo Castro**  
https://drive.google.com/open?id=1irx_K4OD3o83MaE7jHjxtJ2mlj_B7kbN
* **Daniel S�**  
https://drive.google.com/open?id=1rYK5gWXTSHwKVp4sATPOWKuUdMQwyJs0

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
