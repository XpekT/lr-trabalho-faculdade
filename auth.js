const db = require('./db')

module.exports = (username, password) => {

  if(username.length === 0 || password.length === 0) {

    return { status: 403, id: null }

  }

  // Get user from DB
  let user = db.find(element => {
    return element.username === username
  })

  if(user) {

    if(user.password === password) {

      localStorage.setItem('auth', user.id)

      return { status: 200, id: user.id }

    }

  }

  // Fallback
  return { status: 403, id: null }

}
