const LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./localStorage');

const db = require('./db')

module.exports = (id) => {

  // Check if user is already authenticated
  if(localStorage.getItem('auth')) {

    let storedID = localStorage.getItem('auth')

    let user = db.find(element => element.id === id)

    if(user && user.id === storedID) {
      return true
    }

  }

  return false

}
