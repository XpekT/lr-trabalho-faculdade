// Node Media Server
const { NodeMediaCluster } = require('node-media-server')
const numCPUs = require('os').cpus().length;

const config = {
  rtmp: {
    port: 1935,
    chunk_size: 60000,
    gop_cache: true,
    ping: 60,
    ping_timeout: 30,
    mediaroot: './media/'
  },
  http: {
    port : 8000,
    allow_origin: '*'
  },
  cluster: {
    num: numCPUs
  }
}

let nms = new NodeMediaCluster(config)

nms.run()

// Express Server
const path = require('path')
const express = require('express')
const app = express()
const port = 3000
const routes = require('./router')
const bodyParser = require('body-parser')
const reactExpress = require('express-react-views')
const cors = require('cors')

// Middleware
app.use(express.static(path.resolve(__dirname, 'dist')))
app.use(express.static(path.resolve(__dirname, 'media')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
// Express Routes
app.use('/', routes)

app.set('views', path.resolve(__dirname, 'views'))
app.set('view engine', 'jsx');
app.engine('jsx', reactExpress.createEngine());

// Express Listen
app.listen(port, () => console.log(`Servidor a correr na porta: ${port}!`))
