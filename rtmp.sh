#!/usr/bin/env bash

# Root Privileges
su -c 'sh -s' <<EOF

cd ./ffmpeg
./ffmpeg -re -i ../media/the_great_gatsby_behind_the_scenes-480.mp4 -c:v h264 -crf 15 -b:v 2M -maxrate 2M -bufsize 3M -preset ultrafast -tune zerolatency -f flv rtmp://localhost/live/video

EOF
