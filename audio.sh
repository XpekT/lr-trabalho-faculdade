#!/usr/bin/env bash

# Root Privileges
su -c 'sh -s' <<EOF

cd ./ffmpeg
./ffmpeg -re -i ../media/audio/stream.mp3 -codec:a libmp3lame -qscale:a 5 -f flv rtmp://localhost/live/audio

EOF
