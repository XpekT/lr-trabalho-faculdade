const express = require('express')
const router = express.Router()
const auth = require('./auth')
const session = require('./session')
const path = require('path')
const fs = require('fs')

// Media Directory
const mediaRoot = path.resolve(__dirname, 'media')

// Spawns a new Linux Process
const { spawn } = require('child_process')

// Auth
const stupidLocalAuthChecker = (req, res, next) => {

  // Check if user exists and password is OK!
  if(!session(req.params.id)) {

    return res.redirect('/')

  }

  next()

}

// Homepage
router.get('/', (req, res) => res.render('home'))

// Login
router.post('/login', (req, res) => {

  let result = auth(req.body.username, req.body.password)

  result.status === 200 ? res.redirect(`/vod/${result.id}`) : res.redirect('/')

})

// Live Video Stream
router.get('/live/video', (req, res) => {

  const liveDir = './media'
  const streamHash =  'video'

  // Run FFmpeg with elevated privileges
  spawn('./rtmp.sh', {
    stdio: 'inherit',
    shell: true,
    uid: 0
  })

  // Get internal IP address for the qr code url
  const internalIP = require('internal-ip')

  internalIP.v4().then(ip => {

    // Render QR Code
    const QR = require('qr-image')

    // RTMP Link
    let qr_png_rtmp = QR.image(`rtmp://${ip}/live/${streamHash}`, { type: 'png', size: 30 });
    qr_png_rtmp.pipe(fs.createWriteStream(`${liveDir}/live/${streamHash}/qr_code_rtmp_link.png`))

    // Show the qr code and stream
    res.render('live', { ip, streamHash })

  });

})

// Live Audio Stream
router.get('/live/audio', (req, res) => {

  const liveDir = './media'
  const streamHash =  'audio'

  // Run FFmpeg with elevated privileges
  spawn('./audio.sh', {
    stdio: 'inherit',
    shell: true,
    uid: 0
  })

  // Get internal IP address for the qr code url
  const internalIP = require('internal-ip')

  internalIP.v4().then(ip => {

    // Render QR Code
    const QR = require('qr-image')

    // RTMP Link
    let qr_png_rtmp = QR.image(`rtmp://${ip}/live/${streamHash}`, { type: 'png', size: 30 });
    qr_png_rtmp.pipe(fs.createWriteStream(`${liveDir}/live/${streamHash}/qr_code_rtmp_link.png`))

    // Show the qr code and stream
    res.render('live', { ip, streamHash })

  });

})

// VOD
router.get('/vod/:id', stupidLocalAuthChecker, (req, res) => {

  // Get user's ID
  const id = req.params.id

  // Get user's VOD Directory
  const userDir = path.join(mediaRoot, 'vod') + `/${id}`

  // Read and get user's file's name
  const files = fs.readdirSync(userDir)

  res.render('vod', { files, id })

})

// VOD Folders
router.get('/vod/:id/:folder', (req, res) => {

  let folderPath = path.join(__dirname, 'media', 'vod', req.params.id, req.params.folder)
  let mainFolderPath = `/vod/${req.params.id}`

  fs.readdir(folderPath, (err, files) => {

    res.render('vodfolder', { files, mainFolderPath })

  })

})

// IPTV - GET
router.get('/iptv', (req, res) => {

  res.redirect('iptv.html')

})

// IPTV - POST
router.post('/iptv', (req, res) => {

  let parsers = require("playlist-parser");
  let M3U = parsers.M3U;

  let fs = require("fs");
  let playlist = M3U.parse(fs.readFileSync("./media/playlist.m3u", { encoding: "utf8" }));

  let channel = req.body.channel.toUpperCase()
  let regex =  new RegExp(channel)

  let content = []

  content = playlist.filter(el => {

    if(el && el.title) {
      return el.title.match(regex)
    }

  })

  res.json(content)

})

// Redirect to home if 404 occurs
router.get('*', (req, res) => {
  res.redirect('/')
})

module.exports = router
